package com.hitechworm.android.core.data.executor.rx

import com.hitechworm.android.core.data.executor.rx.wrapper.RxDisposableWrapper
import com.hitechworm.android.core.domain.executor.Disposable
import com.hitechworm.android.core.domain.executor.IUseCaseExecutor
import io.reactivex.Observable
import io.reactivex.Scheduler

class ObservableExecutor<T>(var observable: Observable<T>) : AbsRxUseCaseExecutor<T>(), IUseCaseExecutor<T> {

    private var successCallback: ((T) -> Unit)? = null

    private var errorCallback: ((E: Throwable) -> Unit)? = null

    private var completeCallback: (() -> Unit)? = null

    override fun subscribeOn(scheduler: Scheduler): IUseCaseExecutor<T> =
            apply { observable.subscribeOn(scheduler) }

    override fun observeOn(scheduler: Scheduler): IUseCaseExecutor<T> =
            apply { observable.observeOn(scheduler) }

    override fun onSuccess(onSuccess: (T) -> Unit): IUseCaseExecutor<T> =
            apply { successCallback = onSuccess }

    override fun onError(onError: (E: Throwable) -> Unit): IUseCaseExecutor<T> =
            apply { errorCallback = onError }

    override fun onComplete(onComplete: () -> Unit): IUseCaseExecutor<T> =
            apply { completeCallback = onComplete }

    override fun exec(): Disposable {
        return RxDisposableWrapper(observable.subscribe(successCallback, errorCallback, completeCallback))
    }

}