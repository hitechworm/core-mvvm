package com.hitechworm.android.core.data.executor.rx.factory

import com.hitechworm.android.core.data.executor.rx.CompletableExecutor
import com.hitechworm.android.core.data.executor.rx.FlowableExecutor
import com.hitechworm.android.core.data.executor.rx.ObservableExecutor
import com.hitechworm.android.core.data.executor.rx.SingleExecutor
import com.hitechworm.android.core.domain.executor.IExecutorFactory
import com.hitechworm.android.core.domain.executor.IUseCaseExecutor
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

@Suppress("UNCHECKED_CAST")
class RxExecutorFactory : IExecutorFactory {
    override fun <T> getExecutor(param: Any): IUseCaseExecutor<T>? =
            when (param.javaClass) {
                Observable<T>::javaClass -> ObservableExecutor(param as Observable<T>)
                Single<T>::javaClass -> SingleExecutor(param as Single<T>)
                Flowable<T>::javaClass -> FlowableExecutor(param as Flowable<T>)
                Completable::javaClass -> CompletableExecutor(param as Completable)
                else -> null
            }
}