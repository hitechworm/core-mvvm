package com.hitechworm.android.core.data

interface IModelMapper<Input, Output> {
    fun transform(response: Input): Output
}