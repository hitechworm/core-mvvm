package com.hitechworm.android.core.data.executor.rx

import com.hitechworm.android.core.data.executor.rx.wrapper.RxDisposableWrapper
import com.hitechworm.android.core.domain.executor.Disposable
import com.hitechworm.android.core.domain.executor.IUseCaseExecutor
import io.reactivex.Scheduler
import io.reactivex.Single

class SingleExecutor<T>(var single: Single<T>) : AbsRxUseCaseExecutor<T>(), IUseCaseExecutor<T> {

    private var successCallback: ((T) -> Unit)? = null

    private var errorCallback: ((E: Throwable) -> Unit)? = null

    private var completeCallback: (() -> Unit)? = null

    override fun subscribeOn(scheduler: Scheduler): IUseCaseExecutor<T> =
            apply { single.subscribeOn(scheduler) }

    override fun observeOn(scheduler: Scheduler): IUseCaseExecutor<T> =
            apply { single.observeOn(scheduler) }

    override fun onSuccess(onSuccess: (T) -> Unit): IUseCaseExecutor<T> =
            apply { successCallback = onSuccess }

    override fun onError(onError: (E: Throwable) -> Unit): IUseCaseExecutor<T> =
            apply { errorCallback = onError }

    override fun onComplete(onComplete: () -> Unit): IUseCaseExecutor<T> =
            apply { completeCallback = onComplete }

    override fun exec(): Disposable {
        return RxDisposableWrapper(single.subscribe(
                {
                    successCallback?.invoke(it)
                    completeCallback?.invoke()
                }, errorCallback))
    }
}