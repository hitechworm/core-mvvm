package com.hitechworm.android.core.data.executor.rx.wrapper

import io.reactivex.disposables.Disposable


class RxDisposableWrapper(private val disposable: Disposable) : com.hitechworm.android.core.domain.executor.Disposable {

    override fun isDisposed(): Boolean {
        return disposable.isDisposed
    }

    override fun dispose() {
        disposable.dispose()
    }
}