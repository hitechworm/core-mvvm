package com.hitechworm.android.core.data.executor.rx

import com.hitechworm.android.core.domain.executor.ExecuteConfiguration
import com.hitechworm.android.core.domain.executor.IUseCaseExecutor
import com.hitechworm.android.core.domain.executor.ThreadOptions
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

fun ThreadOptions.toScheduler(): Scheduler {
    return when (this) {
        ThreadOptions.IO -> Schedulers.io()
        ThreadOptions.MAIN_THREAD -> Schedulers.trampoline()
        ThreadOptions.COMPUTATION -> Schedulers.computation()
    }
}

abstract class AbsRxUseCaseExecutor<T> : IUseCaseExecutor<T> {

    override fun setup(options: ExecuteConfiguration): IUseCaseExecutor<T> {
        subscribeOn(options.subscribeOn.toScheduler())
        observeOn(options.observeOn.toScheduler())
        return this
    }

    abstract fun subscribeOn(scheduler: Scheduler): IUseCaseExecutor<T>

    abstract fun observeOn(scheduler: Scheduler): IUseCaseExecutor<T>
}