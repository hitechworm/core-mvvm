package com.hitechworm.android.core.app

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseModelImpl : BaseContract.IDataModel {

    private var compositeDisposable = CompositeDisposable()

    override fun subscribe(disposable: Disposable) = compositeDisposable.add(disposable)

    override fun destroy() {
        compositeDisposable.dispose()
        compositeDisposable.clear()
    }
}