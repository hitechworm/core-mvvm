package com.hitechworm.android.core.app.view

import android.support.v7.app.AppCompatActivity

/**
 * Created by Hitechworm on 1/7/18.
 */
abstract class RootActivity : AppCompatActivity()
