package com.hitechworm.android.core.app

import io.reactivex.disposables.Disposable


interface BaseContract {

    interface IDataModel {
        /**
         * must implement this function to make sure operation can be control
         */
        fun subscribe(disposable: Disposable): Boolean

        fun destroy()
    }

    interface IView {

        fun initView()

        fun showLoadingView()

        fun hideLoadingView()

        fun finish()
    }

    interface IViewModel<V : IView, M : IDataModel> {
        fun onViewDestroy()
    }
}