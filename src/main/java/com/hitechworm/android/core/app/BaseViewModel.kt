package com.hitechworm.android.core.app

import android.arch.lifecycle.ViewModel

/**
 * Created by Hitechworm on 1/7/18.
 */

abstract class BaseViewModel<V : BaseContract.IView, M : BaseContract.IDataModel>(var mView: V?, var mModel: M)
    : ViewModel(), BaseContract.IViewModel<V, M> {


    override fun onViewDestroy() {
        mView = null
    }

    override fun onCleared() {
        mModel.destroy()
        super.onCleared()
    }
}