package com.hitechworm.android.core.app.view

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.hitechworm.android.core.app.BaseContract
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by Hitechworm on 1/7/18.
 */
abstract class BaseScreenFragment<Binding : ViewDataBinding, ViewModel : BaseContract.IViewModel<*, *>> :
        Fragment(),
        BaseContract.IView,
        HasSupportFragmentInjector {

    private val mViewModel: ViewModel by lazy { createViewModel() }

    lateinit var navController: NavController

    lateinit var mBinding: Binding

    // DAGGER SETUP
    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate<Binding>(layoutInflater, getLayout(), container, false)
                .apply {
                    setVariable(getBindingViewModelVariableId(), mViewModel)
                    setLifecycleOwner(this@BaseScreenFragment)
                }
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        getToolBar()?.let { NavigationUI.setupWithNavController(it, navController) }
        initView()
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return childFragmentInjector
    }

    abstract fun getLayout(): Int

    abstract fun getToolBar(): Toolbar?

    abstract fun getBindingViewModelVariableId(): Int

    abstract fun createViewModel(): ViewModel

    override fun onDetach() {
        mViewModel.onViewDestroy()
        mBinding.unbind()
        super.onDetach()
    }

    override fun finish() {
        activity?.finish()
    }
}