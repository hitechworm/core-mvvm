@file:Suppress("DEPRECATION")

package com.hitechworm.android.core.app.view

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import com.hitechworm.android.core.app.BaseContract
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasFragmentInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by Hitechworm on 1/7/18.
 */
abstract class BaseScreenActivity<Binding : ViewDataBinding, ViewModel : BaseContract.IViewModel<*, *>> :
        RootActivity(),
        BaseContract.IView,
        HasFragmentInjector,
        HasSupportFragmentInjector {

    @Inject
    lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var frameworkFragmentInjector: DispatchingAndroidInjector<android.app.Fragment>

    private val mViewModel: ViewModel by lazy { createViewModel() }

    lateinit var mBinding: Binding

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return supportFragmentInjector
    }

    override fun fragmentInjector(): AndroidInjector<android.app.Fragment> {
        return frameworkFragmentInjector
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView<Binding>(this, getLayout())
                .apply {
                    setVariable(getBindingViewModelVariableId(), mViewModel)
                    setLifecycleOwner(this@BaseScreenActivity)
                }
        initView()
    }


    abstract fun getLayout(): Int

    abstract fun getBindingViewModelVariableId(): Int

    abstract fun createViewModel(): ViewModel

    override fun onDestroy() {
        mViewModel.onViewDestroy()
        mBinding.unbind()
        super.onDestroy()
    }
}
