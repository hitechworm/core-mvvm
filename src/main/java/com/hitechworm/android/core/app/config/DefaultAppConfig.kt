package com.hitechworm.android.core.app.config

import android.content.Context
import com.orhanobut.hawk.DefaultHawkFacade
import com.orhanobut.hawk.Hawk
import com.orhanobut.hawk.HawkBuilder

class DefaultAppConfig
private constructor(private val hawkFacade: DefaultHawkFacade) : IAppConfig {

    companion object {
        fun create(context: Context, hawkBuilder: HawkBuilder? = null) =
                DefaultAppConfig(DefaultHawkFacade(hawkBuilder ?: Hawk.init(context)))
    }

    override fun <T> put(key: String, value: T): Boolean {
        return hawkFacade.put(key, value)
    }

    override operator fun <T> get(key: String): T {
        return hawkFacade.get(key)
    }

    override operator fun <T> get(key: String, defaultValue: T): T {
        return hawkFacade.get(key, defaultValue)
    }

    override fun count(): Long {
        return hawkFacade.count()
    }

    override fun deleteAll(): Boolean {
        return hawkFacade.deleteAll()
    }

    override fun delete(key: String): Boolean {
        return hawkFacade.delete(key)
    }

    override operator fun contains(key: String): Boolean {
        return hawkFacade.contains(key)
    }

    override fun isBuilt(): Boolean {
        return hawkFacade.isBuilt
    }

    override fun destroy() {
        hawkFacade.destroy()
    }
}