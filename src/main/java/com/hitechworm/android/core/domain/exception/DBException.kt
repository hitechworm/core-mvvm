package com.hitechworm.android.core.domain.exception

class DBException(message: String?, cause: Throwable?) : IOException(message, cause)