package com.hitechworm.android.core.domain.executor

interface IExecutorFactory {
    fun <T> getExecutor(param: Any): IUseCaseExecutor<T>?
}