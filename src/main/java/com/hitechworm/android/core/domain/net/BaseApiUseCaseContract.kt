package com.hitechworm.android.core.domain.net

import com.hitechworm.android.core.domain.IUseCaseCallback

interface IBaseApiUseCaseCallback<Result> : IUseCaseCallback<Result> {
    fun noNetworkConnectionError()
}