package com.hitechworm.android.core.domain.net

import com.hitechworm.android.core.domain.AbsUseCase

abstract class AbsBaseApiUseCase<Params, Result, Callback : IBaseApiUseCaseCallback<Result>> : AbsUseCase<Params, Result, Callback>() {

    override fun handleGenericError(error: Throwable, callback: Callback): Boolean {
        // TODO("not implemented")
        return false
    }
}