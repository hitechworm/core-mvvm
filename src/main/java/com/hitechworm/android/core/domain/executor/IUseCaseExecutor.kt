package com.hitechworm.android.core.domain.executor

interface IUseCaseExecutor<T> {

    fun setup(options: ExecuteConfiguration): IUseCaseExecutor<T>

    fun onSuccess(onSuccess: (T) -> Unit): IUseCaseExecutor<T>

    fun onError(onError: (E: Throwable) -> Unit): IUseCaseExecutor<T>

    fun onComplete(onComplete: () -> Unit): IUseCaseExecutor<T>

    fun exec(): Disposable
}