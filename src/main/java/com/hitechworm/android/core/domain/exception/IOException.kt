package com.hitechworm.android.core.domain.exception

open class IOException(message: String?, cause: Throwable?) : BaseException(message, cause)