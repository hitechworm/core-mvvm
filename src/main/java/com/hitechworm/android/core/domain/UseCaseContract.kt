package com.hitechworm.android.core.domain

import com.hitechworm.android.core.domain.executor.ExecuteConfiguration
import com.hitechworm.android.core.domain.executor.IUseCaseExecutor

interface IUseCase<Params, Result, Callback : IUseCaseCallback<Result>> {

    /**
     * Executes the current use case.
     *
     * by [.createExecutor] ()} method.
     * @param params   Parameters (Optional) used to build/execute this use case.
     */
    fun execute(params: Params, callback: Callback, config: ExecuteConfiguration)

    /**
     * Executes the current use case.
     *
     * by [.createExecutor] ()} method.
     * @param params   Parameters (Optional) used to build/execute this use case.
     */
    fun execute(params: Params, callback: Callback)

    fun createExecutor(params: Params): IUseCaseExecutor<Result>?

    fun handleGenericError(error: Throwable, callback: Callback): Boolean

    fun dispose()
}

interface IUseCaseCallback<ResponseModel> {

    fun onSuccess(response: ResponseModel)

    fun onError(error: Throwable)

    /**
     * call back alway call do not care success or error
     */
    fun onFinish()
}