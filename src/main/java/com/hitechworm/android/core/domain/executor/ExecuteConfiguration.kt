package com.hitechworm.android.core.domain.executor

enum class ThreadOptions { IO, MAIN_THREAD, COMPUTATION }

class ExecuteConfiguration(
        val subscribeOn: ThreadOptions,
        val observeOn: ThreadOptions
) {
    companion object {
        private var defaultOptions: ExecuteConfiguration? = null

        fun default(): ExecuteConfiguration {
            defaultOptions = defaultOptions ?: ExecuteConfiguration(ThreadOptions.IO, ThreadOptions.MAIN_THREAD)
            return defaultOptions!!
        }
    }
}