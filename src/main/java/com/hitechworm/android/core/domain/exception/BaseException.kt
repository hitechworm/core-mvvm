package com.hitechworm.android.core.domain.exception

open class BaseException(message: String?, cause: Throwable?) : Exception(message, cause)