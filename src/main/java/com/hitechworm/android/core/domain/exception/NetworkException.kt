package com.hitechworm.android.core.domain.exception

class NetworkException(message: String?, cause: Throwable?) : IOException(message, cause)