package com.hitechworm.android.core.domain

import com.hitechworm.android.core.domain.exception.ExecutorNotFoundException
import com.hitechworm.android.core.domain.executor.Disposable
import com.hitechworm.android.core.domain.executor.ExecuteConfiguration

/**
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * This interface represents a execution unit for different use cases (this means any use case
 * in the application should implement this contract).
 *
 *
 * By convention each UseCase implementation will return the result using a
 * that will execute its job in a background thread and will post the result in the UI thread.
 */
abstract class AbsUseCase<Params, Result, Callback : IUseCaseCallback<Result>> : IUseCase<Params, Result, Callback> {

    private var disposable: Disposable? = null

    override fun execute(params: Params, callback: Callback) {
        execute(params, callback, ExecuteConfiguration.default())
    }

    override fun execute(params: Params, callback: Callback, config: ExecuteConfiguration) {
        val executor = createExecutor(params)
        if (executor != null) {
            disposable = executor.setup(config)
                    .onSuccess { callback.onSuccess(it) }
                    .onError { handleError(it, callback) }
                    .onComplete { callback.onFinish() }
                    .exec()
        } else {
            callback.onError(ExecutorNotFoundException())
            callback.onFinish()
        }
    }

    private fun handleError(error: Throwable, callback: Callback) {
        if (!handleGenericError(error, callback)) {
            callback.onError(error)
        }
    }

    override fun dispose() {
        disposable?.let {
            if (it.isDisposed()) {
                dispose()
            }
        }
        disposable = null
    }
}
