package com.hitechworm.android.core.domain.executor

interface Disposable {

    /**
     * Returns true if this resource has been disposed.
     * @return true if this resource has been disposed
     */
    fun isDisposed(): Boolean

    /**
     * Dispose the resource, the operation should be idempotent.
     */
    fun dispose()
}