package com.hitechworm.android.widget.input.validation

import java.util.regex.Pattern

const val EMAIL_REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$"
// sample number +00-000-000-0000

class EmailAddressValidationRule(errorMessage: String) : BaseInputValidationRule(errorMessage) {
    override fun isValid(input: CharSequence) =
            Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE).matcher(input).find()
}