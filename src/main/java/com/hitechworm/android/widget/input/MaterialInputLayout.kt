package com.hitechworm.android.widget.input

import android.content.Context
import android.databinding.BindingAdapter
import android.support.design.widget.TextInputLayout
import android.util.AttributeSet
import com.hitechworm.android.widget.input.validation.BaseInputValidationRule
import com.hitechworm.android.widget.input.validation.EmptyValidationRule

@BindingAdapter("text")
fun MaterialInputLayout.setText(text: CharSequence?) {
    editText?.setText(text ?: EMPTY_STRING)
}

@BindingAdapter("validatedTextChange")
fun MaterialInputLayout.setValidatedTextChange(textChangeListener: (String) -> Unit) {
    onValidTextInputListener = textChangeListener
}

@BindingAdapter("requiredError")
fun MaterialInputLayout.setRequiredError(requiredError: String?) =
        if (requiredError == null || requiredError.isEmpty()) {
            fieldRequireValidation?.let { validationRules -= it }
            fieldRequireValidation = null
        } else {
            fieldRequireValidation = EmptyValidationRule(requiredError)
            validationRules += fieldRequireValidation!!
        }

const val EMPTY_STRING = ""

open class MaterialInputLayout : TextInputLayout {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    internal var validationRules: List<BaseInputValidationRule> = mutableListOf()

    internal var fieldRequireValidation: EmptyValidationRule? = null

    var onValidTextInputListener: ((String) -> Unit)? = null

    open fun onValidTextInput(text: String) {
        onValidTextInputListener?.invoke(text)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        editText?.setOnFocusChangeListener { _, focused ->
            if (focused) {
                error = null
            } else {
                onValidTextInput(if (validateTextInput()) getText() else EMPTY_STRING)
            }
        }
    }

    fun getText() = editText?.text?.toString() ?: EMPTY_STRING

    fun isRequired(): Boolean = fieldRequireValidation != null

    fun validateTextInput(): Boolean {
        validationRules.forEach {
            if (!it.isValid(editText!!.text)) {
                error = it.errorMessage
                return false
            }
        }
        return true
    }

    fun isValid(): Boolean {
        validationRules.forEach {
            if (!it.isValid(editText!!.text)) {
                return false
            }
        }
        return true
    }

    fun addValidationRule(vararg rules: BaseInputValidationRule): MaterialInputLayout {
        validationRules += rules
        return this
    }
}