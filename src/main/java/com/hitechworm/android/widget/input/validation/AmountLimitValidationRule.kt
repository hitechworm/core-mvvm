package com.hitechworm.android.widget.input.validation

class AmountLimitValidationRule(var maxErrorMessage: String,
                                var maxValue: Double,
                                val minErrorMessage: String,
                                var minValue: Double = 0.0) : BaseInputValidationRule("") {
    override fun isValid(input: CharSequence): Boolean {
        val amountValue = input.toString().toDoubleOrNull()
        amountValue?.let {
            errorMessage = when {
                it > maxValue -> maxErrorMessage
                it < minValue -> minErrorMessage
                else -> {
                    return true
                }
            }
            return false
        }
        return false
    }
}