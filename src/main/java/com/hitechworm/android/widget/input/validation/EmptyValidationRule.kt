package com.hitechworm.android.widget.input.validation

class EmptyValidationRule(errorMessage: String) : BaseInputValidationRule(errorMessage) {
    override fun isValid(input: CharSequence): Boolean = input.isNotEmpty()
}