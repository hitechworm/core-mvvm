package com.hitechworm.android.widget.input

import android.content.Context
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.InputType
import android.util.AttributeSet
import com.hitechworm.android.widget.input.validation.PhoneNumberValidationRule

class PhoneNumberInputLayout : MaterialInputLayout {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onFinishInflate() {
        super.onFinishInflate()
        addValidationRule(PhoneNumberValidationRule("Please input valid phone number", !isRequired()))
        editText?.addTextChangedListener(PhoneNumberFormattingTextWatcher("VN"))
        editText?.inputType = InputType.TYPE_CLASS_PHONE
    }
}
