package com.hitechworm.android.widget.input

import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import com.hitechworm.android.widget.input.validation.EmailAddressValidationRule
import com.hitechworm.mvvm.app.R

class EmailAddressInputLayout : MaterialInputLayout {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onFinishInflate() {
        super.onFinishInflate()
        addValidationRule(EmailAddressValidationRule(context.getString(R.string.Email_input_invalid_msg)))
        editText?.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
    }
}
