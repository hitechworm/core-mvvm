package com.hitechworm.android.widget.input.validation

abstract class BaseInputValidationRule(var errorMessage: String) {
    abstract fun isValid(input: CharSequence): Boolean
}