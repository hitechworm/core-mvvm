package com.hitechworm.android.widget.input.validation

import java.util.regex.Pattern

const val PHONE_NUMBER_REGEX = "^((\\(?\\+?\\d{1,2}\\)?)\\D?)?(\\d{3})\\D?(\\d{3})\\D?(\\d{4})\$"
// sample number +00-000-000-0000

class PhoneNumberValidationRule(errorMessage: String, private val acceptBlank: Boolean = false) : BaseInputValidationRule(errorMessage) {
    override fun isValid(input: CharSequence) =
            (acceptBlank && input.isEmpty()) || Pattern.compile(PHONE_NUMBER_REGEX).matcher(input).find()
}