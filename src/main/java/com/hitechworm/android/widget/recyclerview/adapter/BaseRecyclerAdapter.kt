package com.hitechworm.android.widget.recyclerview.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

open class BaseRecyclerViewHolder(val dataBinding: ViewDataBinding) : RecyclerView.ViewHolder(dataBinding.root)

abstract class BaseRecyclerAdapter<DB : ViewDataBinding, VH : BaseRecyclerViewHolder>
    : RecyclerView.Adapter<VH>() {

    private var mLayoutInflater: LayoutInflater? = null

    final override fun onBindViewHolder(viewHolder: VH, position: Int) {
        viewHolder.dataBinding.setVariable(getBindingVariableId(), getItem())
    }

    final override fun onCreateViewHolder(parent: ViewGroup, position: Int): VH =
            createViewHolder(DataBindingUtil.inflate(getInflater(parent.context), getItemLayout(), parent, false))

    private fun getInflater(context: Context): LayoutInflater {
        mLayoutInflater = mLayoutInflater ?: LayoutInflater.from(context)
        return mLayoutInflater!!
    }

    abstract fun getItem(): Any

    abstract fun getItemLayout(): Int

    abstract fun getBindingVariableId(): Int

    abstract fun createViewHolder(dataBinding: DB): VH
}