package com.hitechworm.android.utils

object StringUtils {

    private val EMPTY_STRING = ""

    fun isNullOrEmpty(string: String?) = string == null || EMPTY_STRING == string

}
